#include <SPI.h>
#include <Ethernet.h>

#define REDPIN 9 //Assigns pin 9 to control of red value of led strip
#define GREENPIN 6 //Assigns pin 6 to control of green value of led strip
#define BLUEPIN 3 //Assigns pin 3 to control of blue value of led strip

bool ledIsOn; //Changes when led gets turned on or off
bool sRunning = false; //Changes when movementsensor gets turned on or off
bool permanentOverride = false; //Changes when led gets turned on or off via on/off button in app

int pirPin = 8; //Assigns pin 8 to movementsensor
int pirStat = 0; //Movementsensor state
int r, g, b; //stores RGB values
int timer = 10; //Stores timer value received from client

String command; //Stores command received from client

unsigned long currentMillis = 0; //Stores current Millis
unsigned long previousMillis = 0; //Stores the amount of Millis when movementsensor detects movement
unsigned long offTimer = 0; //Stores the amount of Millis when light gets turned off
unsigned long ledTimer = 0; //Stores the amount of Millis when light gets turned on

 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // the media access control (ethernet hardware) address for the shield: 

byte ip[] = { 192,168,2,70 }; //the IP address for the shield:   

byte gateway[] = { 192,168,2,254 }; // the router's gateway address:

byte subnet[] = { 255, 255, 0, 0 }; // the subnet:

EthernetServer server = EthernetServer(80); //Sets the server port to 80

void setup()
{  
  r = 255; //Sets red RGB value to max to default to white
  g = 255; //Sets green RGB value to max to default to white
  b = 255; //Sets green RGB value to max to default to white
  
  Serial.begin(9600); //Starts serial monitor

  pinMode(pirPin, INPUT); //Sets pirPin as an inpur]t

  pinMode(REDPIN, OUTPUT); //Sets REDPIN as an output
  pinMode(GREENPIN, OUTPUT); //Sets GREENPIN as an output
  pinMode(BLUEPIN, OUTPUT); //Sets BLUEPIN as an output
  
  Ethernet.begin(mac, ip, gateway, subnet); //initialize the ethernet device

  server.begin(); //start listening for clients
  
}

void loop()
{  
  currentMillis = millis(); //Sets currentMillis to the amount of millis passed measured by the arduino
  
  EthernetClient client = server.available(); // if an incoming client connects, there will be bytes available to read:
  if (client == true) {
    // read bytes from the incoming client and write them back
    // to any clients connected to the server:
    server.write(client.read());
  }
  if(client){ //Checks wether there is a client or not
    while(client.available() > 0){ //Executes code while there is an available client
      char data = (char)client.read(); //Puts incoming characters from client into char data
      command += data; //Used to convert data to string to be used in checks
      }
      Serial.println(command); //Prints command in serial monitor     
  }

  if(currentMillis - offTimer >= timer * 1000){ //Checks if currentMillis is set amount of millis larger than offTimer
    permanentOverride = false; //Sets permanentOverride to false
  }
  
  if(command == "on"){ //Checks command
    permanentOverride = true; //Sets permanentOverride to true
    ledOn(); //Calls ledOn method
  }

  if(command == "off"){ //Checks command
    ledOff(); //Calls ledOff method
    offTimer = currentMillis; //Sets offTimer to currentMillis
  }

  if(command.indexOf("cTimer") == 0){
    char cTimer[30]; //Creates character array to be used with strtok function
    char *token; //Temporary storage of characters to be used with strtok function
    command.toCharArray(cTimer, 30); //Copy command into cTimer

    token = strtok(cTimer, ","); //stores characters into token up until the first , character
    token = strtok(NULL, ","); //stores characters into token up until the next , character
    timer = atoi(token); //Convert ascii values in token to a int and stores them in timer

    Serial.println(timer);
  }

  if(command.indexOf("cChange") == 0){ //Checks if command contains cChange
    char cChange[30]; //Creates character array to be used with strtok function
    char *token; //Temporary storage of characters to be used with strtok function     
    command.toCharArray(cChange, 30); //Copy command into cChange
    token = strtok(cChange, ","); //stores characters into token up until the first , character
    for(int i = 0; i <=2; i++){ //Makes it store characters into token up until the next , character 3 times
      switch (i){ //Switch case with i as parameter
        case 0: //Checks if i is 0
          token = strtok(NULL, ","); //stores characters into token up until the next , character
          r = atoi(token); //Convert ascii values in token to a int and stores them in r
        break;

        case 1: //Checks if i is 1
          token = strtok(NULL, ","); //stores characters into token up until the next , character
          g = atoi(token); //Convert ascii values in token to a int and stores them in g
        break;

        case 2: //Checks if i is 2
          token = strtok(NULL, ","); //stores characters into token up until the next , character
          b = atoi(token); //Convert ascii values in token to a int and stores them in b
        break;

        default:
        break;
      }
    }
    if(ledIsOn == true){ //Checks if led is on
      ledOn(); //Calls ledOn method
    }
  }
  
  if(command == "sOn"){ //Checks command
    sRunning = true; //Sets sRunning to true
  }

  if(command == "sOff"){ //Checks command
    sRunning = false; //Sets sRunning to false
    if(permanentOverride == false){ //Checks if permanentOverride = false
      ledOff(); //Calls ledOff method
    }
  }

  if(sRunning == true && permanentOverride == false){ //Checks if sRunnig = true and permanentOverride = false
    mSensor(); //Calls mSensor method
    
    if(currentMillis - previousMillis >= timer * 1000){ //Checks if currentMillis is the set amount larger than previousMillis
      ledOff(); //Calls ledOff method
    }
    else{
      ledOn(); //Calls ledOn method
    }
  }

  if(command == "lStatus"){ //Checks command
    if (ledIsOn == true){ //Checks if led is on
      server.println("on"); //Sends "on" to Client
    }else{
      server.println("off"); //Sends "off" to Client
    }
  }

  if(command == "mStatus"){ //Checks command
    if (sRunning == true){ //Checks if movementsensor
      server.println("on"); //Sends "on" to Client
    }else{
      server.println("off"); //Sends "off" to Client
    }
  }

  if(command == "tStatus"){ //Checks command
    if (ledIsOn == true){ //Checks if led is on
      server.println((currentMillis - ledTimer) / 1000); //Sends amount of time led has been on to client
    }
    else{
      server.println("Light is off"); //Sends "Light is off" to client
    }
  }
  
  command = ""; //Clears command
}

void mSensor(){
   pirStat = digitalRead(pirPin);  //set pirStat to pirPin
 if (pirStat == HIGH) { //Checks if pirStat is high
    previousMillis = currentMillis; //Sets previousMillis to currentMillis
 } 
}

void ledOn(){ //turns on led, updates RGB values and starts ledTimer
    analogWrite(REDPIN, r); //Updates red RGB value
    analogWrite(GREENPIN, g); //Updates green RGB value
    analogWrite(BLUEPIN, b); //updates blue RGB value
    if(ledTimer == 0){ //Checks if ledTimer hasn't started
      ledTimer = currentMillis; //Sets ledtimer to currentMillis
    }
    ledIsOn = true; //Sets ledIsOn to true
}

void ledOff(){ //Turns off led
  analogWrite(REDPIN, 0); //Sets red RGB value to 0
  analogWrite(GREENPIN, 0); //Sets green RGB value to 0
  analogWrite(BLUEPIN, 0); //Sets blue RGB value to 0
  ledIsOn = false; //Sets ledIsOn to false
  ledTimer = 0; //Resets ledTimer
}
